import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Restaurante } from "./restaurante.model";
import { environment } from "src/environments/environment";
import { BehaviorSubject } from "rxjs";
import { map, tap } from "rxjs/operators";
@Injectable({
 providedIn: 'root'
})
export class RestauranteService{

  constructor(private http: HttpClient){}

  private _restaurantes = new BehaviorSubject<Restaurante[]>([]);

  get restaurantes(){
    return this._restaurantes.asObservable();
  }

 addRestaurante(restaurante: Restaurante){
    this.http.post<any>(
  environment.firebaseUrl + 'restaurantes.json', {...restaurante}
  )
  .subscribe(data => {
  console.log(data);
 });

  }

  /*getAllRestaurantes(){ //esto se hacia corriendo el programa  antes de quitarlooooOOO

  this.addRestaurante(new Restaurante('1','Carls Jr','https://frankata.com/wp-content/uploads/2019/01/800x600carlsjr-1170x877.jpg?x39585', ['Hamburguesa', 'Papas'] ));      //QUIERO PONER ESTA IMAGEN, NO LA QUE ESTA EN LA BD
 this.addRestaurante(new Restaurante('2','Cabo Grill','https://i.pinimg.com/280x280_RS/e3/1e/e7/e31ee7950607eb55c87e199fd5ab6dd7.jpg', ['Filete', 'Papas'] ));
 this.addRestaurante(new Restaurante('3','Super Salads','https://lh3.googleusercontent.com/proxy/58HXnirLwJAFdqkzCwXw4PdaGyYpQiM4Q0IKNGHwgtwGIo95hvWrXA2Fs7JVSkicsqmbLnDEOvxGpWeOY2lDxPXtPa9smuDJZqIELB9dFz_EkfY1Oa3pSmoDXYNWQBPvx2NIoU1xUbuLBXkBIb-AKkFydyuLE50cpQw4DYAPwiRS73-NHaqJr4InhSQ', ['Ensalada', 'Sandwich'] ));

  }*/

  fetchRestaurantes(){
    return this.http.get<{[key: string] : Restaurante}>(
    environment.firebaseUrl + 'restaurantes.json'
    )
    .pipe(map(dta =>{
    const rests = [];
    for(const key in dta){
      if(dta.hasOwnProperty(key)){
        rests.push(
        new Restaurante(key, dta[key].titulo, dta[key].imgUrl, dta[key].platillos, dta[key].lat, dta[key].lng
        ));
      }
    }
    return rests;
    }),
    tap(rest => {
    this._restaurantes.next(rest);
    }));
    }



 getRestaurante(restauranteId: string){
  const url = environment.firebaseUrl + `restaurantes/${restauranteId}.json`;
  return this.http.get<Restaurante>(url)
  .pipe(map(dta => {
  return new Restaurante(restauranteId, dta.titulo, dta.imgUrl, dta.platillos, dta.lat, dta.lng);
  }));
 }


}
