import { Injectable } from "@angular/core";
import { Ofertas } from "./ofertas.model";
@Injectable({
 providedIn: 'root'
})
export class OfertasService{

 private ofertas: Ofertas[] = [
  {id: 1, titulo: 'Oferta 2x1', imgUrl:'https://i.pinimg.com/originals/73/f3/a1/73f3a18d6d387889db0997472ebb5e76.gif'},
  {id: 2, titulo: 'Oferta 50%', imgUrl:'https://cdn.domestika.org/c_fill,dpr_auto,f_auto,q_auto,w_820/v1542777906/content-items/002/618/992/Gran_oferta_50__de_descuento-01-original.jpg?1542777906'},
  {id: 3, titulo: 'Oferta 30%', imgUrl:'https://previews.123rf.com/images/msvectorplus/msvectorplus1710/msvectorplus171000110/88259461-oferta-especial-30-de-descuento-en-la-mercanc%C3%ADa-vector-icono-rojo-.jpg'}
  ];
 constructor(){}
 getAllOfertas(){
 return [...this.ofertas];
 }
 getOfertas(ofertasId: number){
 return {...this.ofertas.find(r => {
 return r.id === ofertasId;
 })};
 }
}
