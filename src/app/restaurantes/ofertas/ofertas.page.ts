import { Component, OnInit } from '@angular/core';
import { Ofertas } from './ofertas.model';
import { OfertasService } from './ofertas.service';
@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.page.html',
  styleUrls: ['./ofertas.page.scss'],
})
export class OfertasPage implements OnInit {
  ofertas: Ofertas[];

  constructor(private ofertasService: OfertasService) { }

  ngOnInit() {
    this.ofertas = this.ofertasService.getAllOfertas();
  }

}
