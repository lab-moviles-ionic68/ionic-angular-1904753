import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LOCALE_ID} from '@angular/core';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { ReservacionesPageRoutingModule } from './reservaciones-routing.module';

import { ReservacionesPage } from './reservaciones.page';

registerLocaleData(es);
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservacionesPageRoutingModule
  ],
  declarations: [ReservacionesPage],
  providers: [{ provide: LOCALE_ID, useValue: "es-MX"}]
})
export class ReservacionesPageModule {}
