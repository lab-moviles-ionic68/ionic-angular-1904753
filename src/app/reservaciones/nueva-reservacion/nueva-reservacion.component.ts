import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {  ModalController } from '@ionic/angular';
import { Restaurante } from 'src/app/restaurantes/restaurante.model';


@Component({
  selector: 'app-nueva-reservacion',
  templateUrl: './nueva-reservacion.component.html',
  styleUrls: ['./nueva-reservacion.component.scss'],
  providers:[DatePipe]
})
export class NuevaReservacionComponent implements OnInit {
  @Input() restaurante: Restaurante;
  @Input() mode: 'select' | 'hoy';
  fecha: string;
  desdeMin: string;
  Nombre: string;

  customDayShortNames = ['s\u00f8n', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom', 'l\u00f8r'];


  @ViewChild('formNew') myForm: NgForm;

  constructor(
    private modalCtrl: ModalController,
    //private util: Utilities
  ) { }

  ngOnInit() {
    const hoy = new Date();
    this.desdeMin = hoy.toISOString();
    if(this.mode === 'hoy'){
      this.fecha = hoy.toISOString();
    }
  }

  onReservar(){
    this.modalCtrl.dismiss(
      {
      restaurante: this.restaurante,
      nombre: this.myForm.value['nombre'],
      //horario: this.util.castDateToLetter( new Date(this.myForm.value['horario']).toISOString())
      }, 'confirm');
  }

  onCancelar(){ //AQUI EL PDF LO TIENE COMO   onCancel()
    this.modalCtrl.dismiss(null,'cancel');
  }

}
