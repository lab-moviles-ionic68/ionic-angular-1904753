import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonItemSliding, LoadingController } from '@ionic/angular';
import {Reservaciones} from './reservaciones.model'
import { ReservacionesService } from './reservaciones.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-reservaciones',
  templateUrl: './reservaciones.page.html',
  styleUrls: ['./reservaciones.page.scss'],
})
export class ReservacionesPage implements OnInit, OnDestroy {

reservaciones: Reservaciones[]=[];
reservacionesSub: Subscription;
isLoading=false;

constructor(
  private reservacionesService: ReservacionesService,
  private loadingCtrl: LoadingController ) { }

  ngOnInit() {
    this.reservacionesSub = this.reservacionesService.reservaciones.subscribe(rsvs => {
      this.reservaciones = rsvs;
      });
  }

  ionViewWillEnter(){
    console.log('IONIC -> ionViewWillEnter');
      this.isLoading = true;
      this.reservacionesService.fetchReservaciones().subscribe(rsvs => {
      this.reservaciones = rsvs;
      this.isLoading = false;
    });
    }

    //ELIMINAMOS LA SUBSCRIPCION CUANDO ELIMINAMOS LA PAGINA
    ngOnDestroy(){
    if(this.reservacionesSub){
    this.reservacionesSub.unsubscribe();
    }
    }

    onRemoveReservacion(reservacionId: string, slidingEl: IonItemSliding){
      slidingEl.close();
      this.loadingCtrl.create({
      message: 'eliminando reservación ...'
      })
      .then(loadingEl => {
      loadingEl.present();
      this.reservacionesService.removeReservacion(reservacionId).subscribe(() => {
      loadingEl.dismiss();
      });
      });
      }

}
